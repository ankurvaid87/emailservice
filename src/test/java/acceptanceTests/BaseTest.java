package acceptanceTests;

import constants.ApiEndpoints;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class BaseTest {

    @BeforeTest
    @Parameters("env")
    public void initialize(String env){
        ApiEndpoints.setBaseUrl(env);
    }
}
