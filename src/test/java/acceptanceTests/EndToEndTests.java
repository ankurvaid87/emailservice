package acceptanceTests;

import apis.EmailAPI;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.EmailVerification;
import utils.TestData;

import java.util.List;

public class EndToEndTests extends BaseTest{
    private TestData testData;
    private String senderId;
    private String senderUserId;
    private String accountId;
    private String fromName;
    private String senderEmail;

    @BeforeTest
    public void registerNewSender() throws Exception {
        testData = new TestData();
        JSONObject requestPayload = testData.prepareTestDataForRegisterSender("RegisterSender.json");

        Response registerSenderResponse = EmailAPI.registerNewSender(requestPayload);
        JsonPath responseBodyJson = registerSenderResponse.body().jsonPath();

        senderUserId = responseBodyJson.getString("id");
        senderId = responseBodyJson.getString("senderId");
        accountId = responseBodyJson.getString("accountId");
        fromName = responseBodyJson.getString("fromName");
        senderEmail = responseBodyJson.getString("createdBy");

        Assert.assertTrue(testData.getGeneratedUserEmail().equals(senderEmail),
                "The emailId in the response body does not match the generated sender emailId");
        Assert.assertTrue(testData.getGeneratedUserName().equals(fromName),
                "The fromName in the response body does not match the generated sender userName");
    }

    @Test
    public void verifySenderDetails() throws Exception {
        Response response = EmailAPI.getSenderDetails(senderUserId);
        JsonPath responseBodyJson = response.body().jsonPath();

        Assert.assertEquals(responseBodyJson.getString("id"), senderUserId, "Sender UserId does not match the id in response");
        Assert.assertEquals(responseBodyJson.getString("name"), fromName, "Sender name does not match the name in response");
        Assert.assertEquals(responseBodyJson.getString("senderId"), senderId, "SenderId does not match the senderId in response");
        Assert.assertEquals(responseBodyJson.getString("accountId"), accountId, "AccountId does not match the accountId in response");
        Assert.assertEquals(responseBodyJson.getString("createdBy"), senderEmail, "SenderEmail does not match the createdBy in response");
        Assert.assertEquals(responseBodyJson.getString("fromName"), fromName, "FromName does not match the fromName in response");
    }

    @Test
    public void updateSenderDetails() throws Exception {
        testData = new TestData();
        JSONObject requestPayload = testData.prepareTestDataForUpdateSender(fromName, senderEmail);
        Response updateSenderResponse = EmailAPI.updateSenderDetails(requestPayload, senderUserId);
        Assert.assertEquals(updateSenderResponse.statusCode(), 204, "Status code for Update Sender Response does not match 204");

        Response updatedSenderResponse = EmailAPI.getSenderDetails(senderUserId);
        JsonPath responseBodyJson = updatedSenderResponse.body().jsonPath();
        fromName = responseBodyJson.getString("fromName");

        Assert.assertEquals(responseBodyJson.getString("name"), requestPayload.get("name").toString(),
                "Sender name does not match the name in response");
        Assert.assertEquals(responseBodyJson.getString("fromName"), requestPayload.get("fromName").toString(),
                "FromName does not match the fromName in response");
    }

    @Test
    public void deleteSender() throws Exception {
        testData = new TestData();
        JSONObject registerSenderPayload = testData.prepareTestDataForRegisterSender("RegisterSender.json");
        Response registerNewSender = EmailAPI.registerNewSender(registerSenderPayload);
        String newSenderUserId = registerNewSender.body().jsonPath().getString("id");

        Response deleteSenderResponse = EmailAPI.deleteSender(newSenderUserId);
        Assert.assertEquals(deleteSenderResponse.statusCode(), 204, "Status code for delete sender response does not match 204");

        Response getSenderResponse = EmailAPI.getSenderDetails(newSenderUserId);
        Assert.assertEquals(getSenderResponse.statusCode(), 404, "Status code for get sender response does not match 404");
    }

    @Test
    public void sendEmailWithAttachment() throws Exception {
        testData = new TestData();
        JSONObject requestPayload = testData.prepareTestDataForSendEmail("EmailWithAttachment.json", senderId, false);

        Response sendEmailResponse = EmailAPI.sendEmailWithAttachment(senderId, requestPayload);
        String receiverEmail = sendEmailResponse.body().jsonPath().getString("to").replaceAll("\\[", "").replaceAll("]", "");
        String emailSubject = sendEmailResponse.body().jsonPath().getString("subject");
        Assert.assertEquals(sendEmailResponse.statusCode(), 202, "Response code for send email does not match 202");
        Assert.assertTrue(EmailVerification.emailFoundInReceiverInbox(receiverEmail, emailSubject));
    }

    @Test
    public void sendEmailWithTemplate() throws Exception {
        testData = new TestData();
        JSONObject requestPayload = testData.prepareTestDataForSendEmail("EmailWithTemplate.json", senderId, true);

        Response sendEmailResponse = EmailAPI.sendEmailWithTemplate(senderId, requestPayload);
        String receiverEmail = sendEmailResponse.body().jsonPath().getString("to").replaceAll("\\[", "").replaceAll("]", "");
        String emailSubject = testData.getSentEmailSubject();
        Assert.assertEquals(sendEmailResponse.statusCode(), 202, "Response code for send email does not match 202");
        Assert.assertTrue(EmailVerification.emailFoundInReceiverInbox(receiverEmail, emailSubject));
    }

    @Test
    public void resendEmail() throws Exception {
        testData = new TestData();
        JSONObject requestPayload = testData.prepareTestDataForSendEmail("EmailWithAttachment.json", senderId, false);

        Response sendEmailResponse = EmailAPI.sendEmailWithAttachment(senderId, requestPayload);
        String messageId = sendEmailResponse.body().jsonPath().getString("id");

        testData = new TestData();
        JSONObject requestPayloadResendEmail = testData.prepareTestDataForReSendEmail("ResendEmail.json");

        Response resendEmailResponse = EmailAPI.resendEmail(senderId, requestPayloadResendEmail, messageId);
        String receiverEmail = resendEmailResponse.body().jsonPath().getString("to").replaceAll("\\[", "").replaceAll("]", "");
        String emailSubject = resendEmailResponse.body().jsonPath().getString("subject");

        Assert.assertEquals(sendEmailResponse.statusCode(), 202, "Response code for send email does not match 202");
        Assert.assertTrue(EmailVerification.emailFoundInReceiverInbox(receiverEmail, emailSubject));
    }

    @Test
    public void getSentEmail() throws Exception {
        testData = new TestData();
        JSONObject requestPayload = testData.prepareTestDataForSendEmail("EmailWithAttachment.json", senderId, false);

        Response sendEmailResponse = EmailAPI.sendEmailWithAttachment(senderId, requestPayload);
        String messageId = sendEmailResponse.body().jsonPath().getString("id");

        Response getEmailResponse = EmailAPI.getSentEmail(senderId, messageId);

    }
}
