package utils;

import constants.AuthAndCredentials;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Base64;

public class ApiHelper {

    /**
     * Parses JSON from resources and returns a Hashmap
     * @param fileName
     * @return JsonObject
     * @throws IOException
     * @throws ParseException
     */
    public static JSONObject readJSON(String fileName) throws IOException, ParseException {
        File file = new File(ApiHelper.class.getClassLoader().getResource("testData/" + fileName).getFile());

        Reader fileReader = new FileReader(file);
        JSONParser jsonParser = new JSONParser();
        return (JSONObject)jsonParser.parse(fileReader);
    }

    /**
     * Reads authAndCredentials.json and returns Basic Auth
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getBasicAuth() throws IOException, ParseException {
        byte[] decodedByteAuth = Base64.getDecoder().decode(AuthAndCredentials.BASIC_AUTH);

        return new String(decodedByteAuth);
    }

    /**
     * Reads authAndCredentials.json and returns Basic Auth
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getBasicAuthUpdateSender() throws IOException, ParseException {
        byte[] decodedByteAuth = Base64.getDecoder().decode(AuthAndCredentials.BASIC_AUTH_UPDATE_SENDER);

        return new String(decodedByteAuth);
    }
}
