package utils;

import apis.EmailAPI;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class TestData {

    private final String uuid = UUID.randomUUID().toString();
    private final String [] parts = uuid.split("-");
    private final LocalDate date = LocalDate.now();
    private JSONObject jsonBody;
    private String userEmail;
    private String userName;
    private String receiverEmail;
    private static String transactionId;
    private String emailTemplateName;
    private String sentEmailSubject;
    private Logger logger = LogManager.getLogger(TestData.class);

    /**
     * Description : Generates a random email address to be used as test data
     * @return
     */
    public String generateRandomSenderEmail(){
        if(userEmail == null || userEmail.isBlank()){
            String randomEmail = "TestEmailSender" + date + "_" + parts[0] + "@pb.com";
            userEmail = randomEmail;
            return randomEmail;
        }else{
            return userEmail;
        }
    }

    /**
     * Description : Generates a random email address to be used as test data
     * @return
     */
    public String generateRandomReceiverEmail(){
        String randomEmail = "TestEmailReceiver" + date + "_" + parts[0] + "@mailinator.com";
        return randomEmail;
    }

    /**
     * Description : Generates a random transactionId to be used as test data
     * @return
     */
    public static String generateRandomTransactionId(){
        if(transactionId == null || transactionId.isBlank()){
            LocalDate date = LocalDate.now();
            String uuid = UUID.randomUUID().toString();
            String [] parts = uuid.split("-");

            transactionId = date +"_"+ parts[1] + "_v2.5Prov_xid";
            return transactionId;
        }
        return transactionId;
    }

    /**
     * Prepares test data for Register User request payload
     * @param
     * @return
     */
    public JSONObject prepareTestDataForRegisterSender(String fileName) throws IOException, ParseException {
        JSONObject jsonObject = ApiHelper.readJSON(fileName);

        this.userEmail = generateRandomSenderEmail();
        this.userName = userEmail.split("@")[0];

        jsonObject.put("createdBy", this.userEmail);
        jsonObject.put("fromName", this.userName);
        jsonObject.put("name", userName);

        logger.debug("JSON Body for request payload is ->\n" + jsonObject.toJSONString());
        return jsonObject;
    }

    /**
     * Prepares test data for Update Sender request payload
     * @param
     * @return
     */
    public JSONObject prepareTestDataForUpdateSender(String fromName, String createdBy) throws IOException, ParseException {
        JSONObject jsonObject = ApiHelper.readJSON("RegisterSender.json");

        jsonObject.put("fromName", fromName + "_Updated");
        jsonObject.put("name", fromName + "_Updated");
        jsonObject.put("createdBy", createdBy);
        jsonObject.remove("autoHtml");
        jsonObject.remove("autoText");
        jsonObject.remove("trackClicks");
        jsonObject.remove("trackOpens");
        jsonObject.remove("trackingDomain");

        logger.debug("JSON Body for request payload is ->\n" + jsonObject.toJSONString());
        return jsonObject;
    }

    /**
     * Prepares test data for Send Email request payload
     * @param
     * @return
     */
    public JSONObject prepareTestDataForSendEmail(String fileName, String senderId, boolean withTemplate) throws Exception {
        JSONObject jsonObject = ApiHelper.readJSON(fileName);

        this.userEmail = generateRandomSenderEmail();
        this.userName = userEmail.split("@")[0];
        this.receiverEmail = generateRandomReceiverEmail();
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(this.receiverEmail);

        jsonObject.put("from", this.userEmail);
        jsonObject.put("to", jsonArray);
        if(withTemplate){
            Response response = EmailAPI.getEmailTemplates(senderId);
            List<String> templates = response.body().jsonPath().get("name");
            emailTemplateName = templates.get(new Random().nextInt(templates.size()-1));
            getSentEmailSubject(response.body().jsonPath().get("$"), emailTemplateName);
            jsonObject.put("templateName", emailTemplateName);
        }else{
            jsonObject.put("fromName", userName);
            String subject = jsonObject.get("subject").toString();
            jsonObject.put("subject", subject + parts[0]);
        }
        String senderMessageId = jsonObject.get("senderMessageId").toString();
        jsonObject.put("senderMessageId", senderMessageId + parts[0]);

        logger.debug("JSON Body for request payload is ->\n" + jsonObject.toJSONString());
        return jsonObject;
    }

    /**
     * Prepares test data for Resend Email request payload
     * @param
     * @return
     */
    public JSONObject prepareTestDataForReSendEmail(String fileName) throws Exception {
        JSONObject jsonObject = ApiHelper.readJSON(fileName);

        String senderMessageId = jsonObject.get("senderMessageId").toString();
        jsonObject.put("senderMessageId", senderMessageId + parts[0]);

        logger.debug("JSON Body for request payload is ->\n" + jsonObject.toJSONString());
        return jsonObject;
    }

    public String getGeneratedUserEmail(){
        return userEmail;
    }
    public String getGeneratedUserName(){
        return userName;
    }

    /**
     * Returns sent email subject
     * @return
     */
    public String getSentEmailSubject(){
        return sentEmailSubject;
    }

    /**
     * Returns sent email template name
     * @return
     */
    public String getSentEmailTemplateName(){
        return emailTemplateName;
    }

    /**
     * Fetches subject from sent email template
     * @param templates
     * @param templateName
     * @return
     * @throws Exception
     */
    public String getSentEmailSubject(List<LinkedHashMap> templates, String templateName) throws Exception {
        for(int template = 0; template <= templates.size()-1; template++){
            if(templates.get(template).get("name").equals(templateName)){
                sentEmailSubject = templates.get(template).get("subject").toString();
                return sentEmailSubject;
            }
        }
        throw new Exception("Sent Email Subject could not be found from the list of templates for the given sender");
    }

}

