package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class EmailVerification {
    private static WebDriver driver;
    private static String inboxSearchBar = "input#search";
    private static String sendersName = ".table-striped.jambo_table tr>td:nth-of-type(2)";
    private static String emailSubject = ".table-striped.jambo_table tr>td:nth-of-type(3)";
    private static String emailTime = ".table-striped.jambo_table tr>td:nth-of-type(4)";

    /**
     * Launches Headless Chrome Browser
     */
    public static void launchHeadlessBrowser() throws InterruptedException, MalformedURLException {
        // Getting the absolute path of chromedriver.exe from the test resources
        File file = new File(ClassLoader.getSystemClassLoader().getResource("chromedriver.exe").getFile());
        String chromeDriverPath = file.getAbsolutePath();

        // Setting system properties for chrome browser
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors",
                "--disable-extensions","--no-sandbox","--disable-dev-shm-usage");
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        desiredCapabilities.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
//        URL url = new URL("http://localhost:3000");
        driver = new ChromeDriver(desiredCapabilities);
    }

    /**
     * Finds and returns a WebElement
     * @param elementLocator
     * @return
     */
    public static WebElement findElementByCss(String elementLocator){
        return driver.findElement(By.cssSelector(elementLocator));
    }

    /**
     * Waits for the visibility of element on the page for a max of 10 seconds
     * @param element
     */
    public static void waitForElementDisplayed(By element){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    /**
     * Enters receivers email into search box and navigates to inbox
     * @param receiverEmail
     */
    public static void goToMailinatorInbox(String receiverEmail){
        driver.get("https://www.mailinator.com");
        waitForElementDisplayed(By.cssSelector(inboxSearchBar));
        findElementByCss(inboxSearchBar).sendKeys(receiverEmail);
        findElementByCss(inboxSearchBar).sendKeys(Keys.ENTER);
    }

    /**
     * Checks if sent email is found in receivers inbox
     * @param receiverEmail
     * @param subject
     * @return
     * @throws InterruptedException
     */
    public static boolean emailFoundInReceiverInbox(String receiverEmail, String subject) throws InterruptedException, MalformedURLException {
        launchHeadlessBrowser();
        goToMailinatorInbox(receiverEmail);

        for(int i = 0; i <= 5; i++){
            waitForElementDisplayed(By.cssSelector(emailSubject));

            String mailSubject = findElementByCss(emailSubject).getText().trim();
            if(mailSubject.equalsIgnoreCase(subject)){
                driver.quit();
                return true;
            }
            driver.navigate().refresh();
        }
        driver.quit();
        return false;
    }

//    public static void main(String[] args) throws InterruptedException {
//        File file = new File(ClassLoader.getSystemClassLoader().getResource("chromedriver.exe").getFile());
//        String chromeDriverPath = file.getAbsolutePath();
//
//        // Setting system properties for chrome browser
//        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//
//        emailFoundInReceiverInbox("Ankur Vaid", "testdevacc@mailinator.com", "estMailinator");
//
//    }
}
