package constants;

public class AuthAndCredentials {
    public static final String BASIC_AUTH = "QmFzaWMgWVhCcGJUcFRZVUJ6TFdWdFFERnNjeUJoY0dsdElIQmhjM04zYjNKaw==";
    public static final String BASIC_AUTH_UPDATE_SENDER = "QmFzaWMgWVdSdGFXNDZjMkZoY3kxbGJVQXhiSE09";
}
