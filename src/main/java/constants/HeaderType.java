package constants;

public final class HeaderType {
    public static final String X_PB_TRANSACTION_ID = "X-PB-TransactionID";
    public static final String AUTHORIZATION = "Authorization";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String X_PB_SENDER_ID = "X-PB-SenderId";
}
