package constants;

import apis.EmailAPI;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApiEndpoints {
    private static final Logger logger = LogManager.getLogger(EmailAPI.class);
    private static String BASE_URL = "";
    public static final String SENDER = "/sender";
    public static final String BLACKLIST = "/blacklist";
    public static final String EMAIL = "/email";
    public static final String EMAIL_WITH_TEMPLATE = "/email/template";
    public static final String DELIVERIES = "/deliveries";
    public static final String RESEND_EMAIL = "/resend";
    public static final String TEMPLATE = "/template";
    public static final String DELETE_TEMPLATE = "/template/delete";

    /**
     * Sets value of BaseURL (default value = dev)
     * @param env
     */
    public static void setBaseUrl(String env){
        switch (env){
            case "dev":
            case "qa":
            case "stg":
                ApiEndpoints.BASE_URL = "https://email-" + env + ".saase2e.pitneycloud.com/api/v1";
                break;

            case "ppd":
            case "prod":
                ApiEndpoints.BASE_URL = "https://email-" + env + ".saase2e.pitneybowes.com/api/v1";
                break;

            default:
                logger.warn("No env found in commandline variables OR unable to read value of env, hence setting env to dev");
                ApiEndpoints.BASE_URL = "https://email-dev.saase2e.pitneycloud.com/api/v1";
        }

    }

    /**
     * Returns BaseURL
     * @return
     */
    public static String getBaseUrl(){
        return ApiEndpoints.BASE_URL;
    }
}
