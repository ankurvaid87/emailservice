package apis;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.Map;

public class Request {

    private final RequestSpecification httpRequest;
    private final String baseUrl;
    private String relativeUrl;
    private Method requestMethod;
    private final Logger logger = LogManager.getLogger(Request.class);

    public Request(String baseUrl) {
        this.baseUrl = baseUrl;
        RestAssured.baseURI = baseUrl;
        httpRequest = RestAssured.given();
    }

    /**
     * Sets the request endpoint
     *
     * @param relativeUrl request endpoint
     * @return api.Request object
     */
    public Request setEndPoint(String relativeUrl) {
        this.relativeUrl = relativeUrl;
        return this;
    }

    /**
     * Sets the request method
     *
     * @param method request method
     * @return api.Request object
     */
    public Request setMethod(Method method) {
        requestMethod = method;
        return this;
    }

    /**
     * Sets the request header
     *
     * @param name  name of header
     * @param value value of header
     * @return api.Request object
     */
    public Request setHeader(String name, String value) {
        this.httpRequest.header(name, value);
        return this;
    }

    /**
     * Sets the request header
     *
     * @param header map of key,value pairs
     * @return api.Request object
     */
    public Request setHeader(Map<String, String> header) {
        for (Map.Entry<String, String> entry : header.entrySet()) {
            this.setHeader(entry.getKey(), entry.getValue());
        }
        return this;
    }

    /**
     * Sets the request queryParam
     *
     * @param name  name of queryParam
     * @param value value of queryParam
     * @return api.Request object
     */
    public Request setQueryParam(String name, String value) {
        this.httpRequest.queryParam(name, value);
        return this;
    }

    /**
     * Sets the request queryParams
     *
     * @param queryParams map of key,value pairs
     * @return api.Request object
     */
    public Request setQueryParams(Map<String, String> queryParams) {
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            this.setQueryParam(entry.getKey(), entry.getValue());
        }
        return this;
    }


    /**
     * Sets the request body as form params
     *
     * @param params Hashmap of form params
     * @return api.Request object
     */
    public Request setBodyParams(Map<String, ?> params) {
        httpRequest.formParams(params);
        return this;
    }

    /**
     * Sets the request body as json
     *
     * @param jsonBody body as json object
     * @return api.Request object
     */
    public Request setBodyJson(JSONObject jsonBody) {
        httpRequest.body(jsonBody.toJSONString());
        return this;
    }

    public Request setBasicAuth(String username, String password) {
        httpRequest.auth().basic(username, password);
        return this;
    }

    /**
     * Executes the http(s) request
     *
     * @return Response Object
     */
    public Response execute() throws Exception {
        logger.info("Sending " + requestMethod + " request to -> " + baseUrl + relativeUrl);
        Response response =  httpRequest.request(requestMethod, relativeUrl);

        if(response.statusCode() >= 400 && response.statusCode() != 404){
            logger.debug("Response received from " + baseUrl + relativeUrl + " :\n" + response.body().jsonPath().prettify());
            throw new Exception("Request to " + baseUrl+relativeUrl + " returned " + response.statusCode() + " response");
        }else if(response.statusCode() > 201 && response.statusCode() < 400 && !response.body().asString().isBlank()){
            logger.warn("Received " + response.statusCode() + " response from " + baseUrl + relativeUrl + "\n"
                    + response.body().jsonPath().prettify());
            return response;
        }

        if(!response.body().asString().isBlank()){
            logger.debug("Response received from " + baseUrl + relativeUrl + " :\n" + response.body().jsonPath().prettify());
        }
        return response;
    }
}