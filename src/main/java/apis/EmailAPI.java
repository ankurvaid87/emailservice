package apis;

import constants.ApiEndpoints;
import constants.AuthAndCredentials;
import constants.HeaderType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import utils.ApiHelper;
import utils.TestData;

import java.io.IOException;
import java.util.HashMap;

public class EmailAPI {
    private static final Logger logger = LogManager.getLogger(EmailAPI.class);
    private static String senderId;
    private static String senderUserId;

    /**
     * Creates and returns default headers for request as a key-value pair (HashMap)
     * @return
     * @throws IOException
     * @throws ParseException
     */
    private static HashMap setDefaultHeaders() throws IOException, ParseException {
        String debugHeaders = "Request Headers ->  ";

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put(HeaderType.CONTENT_TYPE, "application/json");
        debugHeaders += "Content-Type : " + headers.get("Content-Type") + "\t";
        headers.put(HeaderType.AUTHORIZATION, ApiHelper.getBasicAuth());
        debugHeaders += "Authorization : " + headers.get("Authorization") + "\t";
        headers.put(HeaderType.X_PB_TRANSACTION_ID, TestData.generateRandomTransactionId());
        debugHeaders += "X-PB-TransactionID : " + headers.get("X-PB-TransactionID");
        logger.debug(debugHeaders);

        return headers;
    }

    /**
     * Creates and returns headers for update request as a key-value pair (HashMap)
     * @return
     * @throws IOException
     * @throws ParseException
     */
    private static HashMap setHeadersUpdateSender() throws IOException, ParseException {
        HashMap headers = setDefaultHeaders();
        String debugHeaders = "Request Headers ->  ";

        headers.put(HeaderType.AUTHORIZATION, ApiHelper.getBasicAuthUpdateSender());
        debugHeaders += "Authorization : " + headers.get("Authorization") + "\t";
        logger.debug(debugHeaders);

        return headers;
    }

    /**
     * Sets headers for Send Email request
     * @param senderId
     * @return
     * @throws IOException
     * @throws ParseException
     */
    private static HashMap setHeadersSendEmail(String senderId) throws IOException, ParseException {
        HashMap headers = setDefaultHeaders();
        headers.put("X-PB-SenderId", senderId);
        logger.debug(headers);
        return headers;
    }

    /**
     * Executes a POST request for Registering a New Sender
     * @param payload
     * @return
     * @throws Exception
     */
    public static Response registerNewSender(JSONObject payload) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.SENDER)
                .setHeader(setDefaultHeaders())
                .setMethod(Method.POST)
                .setBodyJson(payload)
                .execute();
        return response;
    }

    /**
     * Executes a GET request for fetching details of a registered sender
     * @return
     * @throws Exception
     */
    public static Response getSenderDetails(String senderUserId) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.SENDER + "/" + senderUserId)
                .setMethod(Method.GET)
                .setHeader(setDefaultHeaders())
                .execute();
        return response;
    }

    /**
     * Executes a PUT request for updating details of a registered sender
     * @return
     * @throws Exception
     */
    public static Response updateSenderDetails(JSONObject requestPayload, String senderUserId) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.SENDER + "/" + senderUserId)
                .setMethod(Method.PUT)
                .setHeader(setHeadersUpdateSender())
                .setBodyJson(requestPayload)
                .execute();
        return response;
    }

    /**
     * Executes a DELETE request for deleting a registered sender
     * @return
     * @throws Exception
     */
    public static Response deleteSender(String senderUserId) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.SENDER + "/" + senderUserId)
                .setMethod(Method.DELETE)
                .setHeader(setHeadersUpdateSender())
                .execute();
        return response;
    }

    /**
     * Executes a POST request for Sending an email with attachment
     * @param senderId
     * @param requestPayload
     * @return
     * @throws Exception
     */
    public static Response sendEmailWithAttachment(String senderId, JSONObject requestPayload) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.EMAIL)
                .setMethod(Method.POST)
                .setHeader(setHeadersSendEmail(senderId))
                .setBodyJson(requestPayload)
                .execute();
        return response;
    }

    /**
     * Fetches and returns email templates for a sender
     * @param senderId
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static Response getEmailTemplates(String senderId) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.TEMPLATE)
                .setHeader(setHeadersSendEmail(senderId))
                .setMethod(Method.GET)
                .execute();
        return response;
    }

    /**
     * Executes a POST request for Sending an email with template
     * @param senderId
     * @param requestPayload
     * @return
     * @throws Exception
     */
    public static Response sendEmailWithTemplate(String senderId, JSONObject requestPayload) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint(ApiEndpoints.EMAIL_WITH_TEMPLATE)
                .setMethod(Method.POST)
                .setHeader(setHeadersSendEmail(senderId))
                .setBodyJson(requestPayload)
                .execute();
        return response;

    }

    /**
     * Executes a POST request for Sending an email with template
     * @param senderId
     * @param messageId
     * @return
     * @throws Exception
     */
    public static Response resendEmail(String senderId, JSONObject requestPayload, String messageId) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint("/email/" + messageId + ApiEndpoints.RESEND_EMAIL)
                .setMethod(Method.POST)
                .setHeader(setHeadersSendEmail(senderId))
                .setBodyJson(requestPayload)
                .execute();
        return response;

    }

    /**
     * Executes a POST request for Sending an email with template
     * @param senderId
     * @param messageId
     * @return
     * @throws Exception
     */
    public static Response getSentEmail(String senderId, String messageId) throws Exception {
        Response response = new Request(ApiEndpoints.getBaseUrl())
                .setEndPoint("/email/" + messageId)
                .setMethod(Method.GET)
                .setHeader(setHeadersSendEmail(senderId))
                .execute();
        return response;

    }
}
